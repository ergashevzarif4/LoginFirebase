/*
package com.example.mruzbek.loginfirebase.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.example.mruzbek.loginfirebase.R
import com.example.mruzbek.loginfirebase.commons.isEmail
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.reg_activity.*
import kotlinx.android.synthetic.main.reg_activity_2.*

class RegActivity : AppCompatActivity() {
    private val mAuther by lazy {
        FirebaseAuth.getInstance()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.reg_activity_2)
        abouth_btn_sig_in.setOnClickListener {


            val email = et_mail.text.toString().trim()
            val password = et_pass.text.toString().trim()
            val password_2 = et_pass_2.text.toString().trim()


            if (TextUtils.isEmpty(email)) {
                Toast.makeText(this, "Enter email address!", Toast.LENGTH_SHORT).show();
            }

            if (TextUtils.isEmpty(password)) {
                Toast.makeText(this, "Enter password!", Toast.LENGTH_SHORT).show();
            }
            if (password != password_2) {
                Toast.makeText(this, "Password No Equals", Toast.LENGTH_SHORT).show();
            }
            if (String().isEmail(email) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(email) && password == password_2) {
                mAuther.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this) { task ->
                            if (task.isSuccessful) {
                                Log.d("TAG", "createUserWithEmail:success")
                                val user = mAuther.currentUser
                                updateUI(user)
                            } else {
                                Log.d("TAG", "createUserWithEmail:failure", task.exception)
                                Toast.makeText(this@RegActivity, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show()
                                updateUI(null)
                            }

                        }

            } else {
                Toast.makeText(this@RegActivity
                        , "Email field is incorrect",
                        Toast.LENGTH_SHORT).show()

            }
        }
    }

    private fun updateUI(user: FirebaseUser?) {

        if (user != null) {
            startActivity(Intent(this, SuccessActivity::class.java))
            finish()
        }
    }
}
*/
