package com.example.mruzbek.loginfirebase.presentation.reg

import android.arch.lifecycle.ViewModel
import android.text.TextUtils
import android.view.View
import com.example.mruzbek.loginfirebase.commons.isEmail

class RegPresenterImpl : ViewModel(), IRegPresenter {
    private lateinit var regview: IRegView
    fun init(iRegView: IRegView) {
        regview = iRegView
    }

    override fun reg(email: String, password: String, password_2: String) {
        if (regview.checkNetState()) {
            if (TextUtils.isEmpty(email)) {
                regview.showError("Enter email address!")
            } else if (TextUtils.isEmpty(password)) {
                regview.showError("Enter password!")

            } else if (password != password_2) {
                regview.showError("Password No Equals")
            } else if (!String().isEmail(email)) {
                regview.showError("Email field is incorrect")
            } else {
                regview.showProgresBar()
                regview.createUserWithEmailAndPassword(email, password)
            }
        } else {
            regview.showError("No internet connection", View.OnClickListener {
                regview.openNetworkSetting()
            })
        }
    }
}