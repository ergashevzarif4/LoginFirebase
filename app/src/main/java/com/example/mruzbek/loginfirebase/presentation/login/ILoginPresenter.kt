package com.example.mruzbek.loginfirebase.presentation.login

interface ILoginPresenter {
    fun onLogin(email: String, password: String)
}