package com.example.mruzbek.loginfirebase.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.mruzbek.loginfirebase.R
import com.example.mruzbek.loginfirebase.presentation.IMainView
import com.example.mruzbek.loginfirebase.presentation.secundnomer.ISecView
import com.example.mruzbek.loginfirebase.presentation.secundnomer.SecPresenterImpl

class SecundNomerFragment : Fragment(), ISecView {
    private lateinit var textView: TextView
    private var mainView: IMainView? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is IMainView)
            mainView = context
    }

    override fun setTVTime(time: String) {
        textView.text = time
    }

    override fun destroyFragment() {
        mainView?.replaseFragment()
    }

    override fun onDetach() {
        super.onDetach()
        secPresenter = null
    }

    private var secPresenter: SecPresenterImpl? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = LayoutInflater.from(container?.context).inflate(R.layout.secundnomer_layout, container, false)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textView = view.findViewById(R.id.tv_secund)
        secPresenter = ViewModelProviders.of(this).get(SecPresenterImpl::class.java)
        secPresenter?.init(this)
        secPresenter?.onTick()

    }

}