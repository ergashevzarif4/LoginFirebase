package com.example.mruzbek.loginfirebase.commons

import android.content.Context
import android.net.ConnectivityManager
import android.support.design.widget.Snackbar
import android.view.View
import android.view.ViewGroup
import java.util.regex.Pattern

fun String.isEmail(it: String): Boolean {
    val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
    val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
    val matcher = pattern.matcher(it)
    return matcher.matches()
}

fun ViewGroup.showError(message: String, listener: View.OnClickListener? = null) {
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).apply {
        if (listener != null)
            setAction("try Again", listener)
        show()
    }
}

fun ViewGroup.showError(messageRes: Int, listener: View.OnClickListener? = null) {
    Snackbar.make(this, messageRes, Snackbar.LENGTH_LONG).apply {
        if (listener != null)
            setAction("try Again", listener)
        show()
    }
}

fun Context.checkNetState(): Boolean {
    val manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    return manager.activeNetworkInfo != null
}


