package com.example.mruzbek.loginfirebase

import android.app.Application
import com.google.firebase.auth.FirebaseAuth

class App : Application() {

    private val mAuther by lazy {
        FirebaseAuth.getInstance()
    }

    override fun onCreate() {
        super.onCreate()
    }

    fun getAuther() = mAuther
}