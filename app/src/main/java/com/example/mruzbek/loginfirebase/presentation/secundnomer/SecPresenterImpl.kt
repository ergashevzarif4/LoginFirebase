package com.example.mruzbek.loginfirebase.presentation.secundnomer

import android.arch.lifecycle.ViewModel
import android.os.CountDownTimer
import java.text.SimpleDateFormat
import java.util.*

class SecPresenterImpl() : ViewModel(), ISePresenter {
    private lateinit var view: ISecView
    private val dateFormat = SimpleDateFormat("ss:SSS")
    fun init(iSecView: ISecView) {
        view = iSecView
    }

    override fun onTick() {
        object : CountDownTimer(10000, 50) {
            override fun onFinish() {
                view.destroyFragment()
            }

            override fun onTick(millisUntilFinished: Long) {
                view.setTVTime(dateFormat.format(millisUntilFinished))

            }

        }.start()
    }
}