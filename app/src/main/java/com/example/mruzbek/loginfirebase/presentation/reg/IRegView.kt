package com.example.mruzbek.loginfirebase.presentation.reg

import android.view.View

interface IRegView {
    fun showProgresBar()
    fun hideProgresBar()
    fun showError(message: String, listener: View.OnClickListener? = null)
    fun showError(messageRes: Int, listener: View.OnClickListener? = null)
    fun createUserWithEmailAndPassword(email: String, password: String)
    fun checkNetState(): Boolean
    fun openNetworkSetting()
}