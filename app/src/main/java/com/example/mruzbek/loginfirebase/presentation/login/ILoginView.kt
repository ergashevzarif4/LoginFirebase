package com.example.mruzbek.loginfirebase.presentation.login

import android.view.View

interface ILoginView {
    fun showProgresBar()
    fun hideProgresBar()
    fun showError(message: String, listener: View.OnClickListener? = null)
    fun showError(messageRes: Int, listener: View.OnClickListener? = null)
    fun onSucces()
    fun checkNetState(): Boolean
    fun openNetworkSetting()
    fun signInWithEmailAndPassword(email: String, password: String)

}