package com.example.mruzbek.loginfirebase.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.mruzbek.loginfirebase.R
import com.example.mruzbek.loginfirebase.commons.checkNetState
import com.example.mruzbek.loginfirebase.commons.showError
import com.example.mruzbek.loginfirebase.presentation.IMainView
import com.example.mruzbek.loginfirebase.presentation.login.ILoginView
import com.example.mruzbek.loginfirebase.presentation.login.LoginPresenterImpl
import kotlinx.android.synthetic.main.login_activity.*
import kotlinx.android.synthetic.main.login_activity_2.*

class LoginFragment : Fragment(), ILoginView {


    private var listener: IMainView? = null
    private var presenter: LoginPresenterImpl? = null
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context != null)
            listener = context as IMainView

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.login_activity_2, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = ViewModelProviders.of(this).get(LoginPresenterImpl::class.java)
        presenter?.init(this)
        abouth_btn_sig_in.setOnClickListener {
            presenter?.onLogin(et_mail.text.toString().trim(), et_pass.text.toString().trim())
        }
        btn_reg.setOnClickListener { listener?.reg() }
    }

    override fun showProgresBar() = pb.setVisibility(View.VISIBLE)


    override fun hideProgresBar() = pb.setVisibility(View.INVISIBLE)


    override fun showError(message: String, listener: View.OnClickListener?) = rootlayout.showError(message, listener)

    override fun showError(messageRes: Int, listener: View.OnClickListener?) = rootlayout.showError(messageRes, listener)

    override fun onSucces() = listener?.onLogin() ?: Unit

    override fun checkNetState(): Boolean = context?.checkNetState() ?: false

    override fun signInWithEmailAndPassword(email: String, password: String) = listener?.signInWithEmailAndPassword(email, password)
            ?: Unit

    override fun openNetworkSetting() = listener?.openNetworkSettings() ?: Unit

    override fun onDetach() {
        super.onDetach()
        listener = null
        presenter = null
    }
}
