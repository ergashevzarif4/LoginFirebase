package com.example.mruzbek.loginfirebase.presentation.login

import android.arch.lifecycle.ViewModel
import android.text.TextUtils
import android.view.View
import com.example.mruzbek.loginfirebase.commons.isEmail

class LoginPresenterImpl : ViewModel(), ILoginPresenter {
    private lateinit var view: ILoginView

    fun init(iLoginView: ILoginView) {
        view = iLoginView
    }


    override fun onLogin(email: String, password: String) {
        if (view.checkNetState()) {
            view.showProgresBar()
            if (TextUtils.isEmpty(email)) {
                view.showError("Enter email address!")
            } else if (TextUtils.isEmpty(password)) {
                view.showError("Enter password!")
            } else if (!String().isEmail(email)) {
                view.showError("Email field is incorrect")
            } else {
                view.showProgresBar()
                view.signInWithEmailAndPassword(email, password)
            }
        } else {
            view.showError("No internet connection", View.OnClickListener {
                view.openNetworkSetting()
            })
        }
    }
}
