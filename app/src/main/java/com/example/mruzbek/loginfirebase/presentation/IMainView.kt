package com.example.mruzbek.loginfirebase.presentation

interface IMainView {
    fun replaseFragment()
    fun onLogin()

    fun reg()

    fun signInWithEmailAndPassword(email: String, password: String)

    fun createUserWithEmailAndPassword(email: String, password: String)

    fun openNetworkSettings()
}