package com.example.mruzbek.loginfirebase.ui.activity

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.example.mruzbek.loginfirebase.App
import com.example.mruzbek.loginfirebase.R
import com.example.mruzbek.loginfirebase.presentation.IMainView
import com.example.mruzbek.loginfirebase.ui.fragment.LoginFragment
import com.example.mruzbek.loginfirebase.ui.fragment.RegFrament
import com.example.mruzbek.loginfirebase.ui.fragment.SecundNomerFragment
import com.google.firebase.auth.FirebaseUser


class MainActivity : AppCompatActivity(), IMainView {
    override fun replaseFragment() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.coontaner, LoginFragment())
                .commit()
    }

    private val mAuther by lazy {
        (application as App).getAuther()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.coontaner, LoginFragment())
                    .commit()
        }

    }

    override fun openNetworkSettings() = startActivity(Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS))

    override fun onLogin() = startActivity(Intent(this, SuccessActivity::class.java))

    override fun reg() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.coontaner, RegFrament())
                .addToBackStack(null)
                .commit()
    }


    override fun signInWithEmailAndPassword(email: String, password: String) {
        mAuther.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this)
                { task ->
                    if (task.isSuccessful) {
                        val user = mAuther.currentUser
                        updateUI(user)
                    } else {
                        Log.d("TAG", "createUserWithEmail:failure", task.exception)
                        Toast.makeText(this@MainActivity, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                        updateUI(null)

                    }
                }
    }

    override fun createUserWithEmailAndPassword(email: String, password: String) {
        mAuther.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Log.d("TAG", "createUserWithEmail:success")
                        val user = mAuther.currentUser
                        updateUI(user)
                    } else {
                        Log.d("TAG", "createUserWithEmail:failure", task.exception)
                        Toast.makeText(this@MainActivity, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                        updateUI(null)
                    }

                }
    }

    var counter: Int = 0
    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            startActivity(Intent(this, SuccessActivity::class.java))
            finish()
        } else {
            counter++
            if (counter == 5) {
                counter = 0
                supportFragmentManager.beginTransaction().add(R.id.coontaner, SecundNomerFragment()).commit()
            }
        }
    }
}
