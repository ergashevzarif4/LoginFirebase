package com.example.mruzbek.loginfirebase.presentation.reg

interface IRegPresenter {
    fun reg(email: String, password: String, pass2: String)
}