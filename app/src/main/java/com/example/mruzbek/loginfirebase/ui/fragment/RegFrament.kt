package com.example.mruzbek.loginfirebase.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.mruzbek.loginfirebase.R
import com.example.mruzbek.loginfirebase.commons.checkNetState
import com.example.mruzbek.loginfirebase.commons.showError
import com.example.mruzbek.loginfirebase.presentation.IMainView
import com.example.mruzbek.loginfirebase.presentation.reg.IRegView
import com.example.mruzbek.loginfirebase.presentation.reg.RegPresenterImpl
import kotlinx.android.synthetic.main.reg_activity.*
import kotlinx.android.synthetic.main.reg_activity_2.*

class RegFrament : Fragment(), IRegView {


    private var regPresenter: RegPresenterImpl? = null
    private var mainView: IMainView? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is IMainView)
            mainView = context
    }

    override fun onCreateView(i: LayoutInflater,
                              c: ViewGroup?,
                              s: Bundle?) =
            i.inflate(R.layout.reg_activity_2, c, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        regPresenter = ViewModelProviders.of(this).get(RegPresenterImpl::class.java)
        regPresenter?.init(this)
        abouth_btn_sig_in.setOnClickListener { regPresenter?.reg(et_mail.text.toString().trim(), et_pass.text.toString().trim(), et_pass_2.text.toString().trim()) }

    }

    override fun showProgresBar() = pb.setVisibility(View.VISIBLE)

    override fun hideProgresBar() = pb.setVisibility(View.INVISIBLE)

    override fun showError(message: String, listener: View.OnClickListener?) = rootlayout.showError(message, listener)

    override fun showError(messageRes: Int, listener: View.OnClickListener?) = rootlayout.showError(messageRes, listener)


    override fun createUserWithEmailAndPassword(email: String, password: String) = mainView?.createUserWithEmailAndPassword(email, password)
            ?: Unit

    override fun checkNetState() = context?.checkNetState() ?: false

    override fun openNetworkSetting() = mainView?.openNetworkSettings() ?: Unit

    override fun onDetach() {
        super.onDetach()
        mainView=null
        regPresenter=null
    }

}